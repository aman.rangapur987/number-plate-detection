# Number Plate Detection
## Project Description
- **Domain** - Security and Surveillance
- **Objective** - To prevent unknown and potentially suspicious vehicles from trespassing into certain restricted premises.
- **Overview** -
A camera will capture the picture of the number plate of the vehicle. If the license plate number is present in the database then the access will be granted. If not, the app will ask the user either to grant one time access or add the number into database and give a permanent access. If no access is to be granted, the vehicle will be detected as a trespasser and will be forbidden from entering.
- **Application areas** - Housing societies,corporate offices and IT parks etc.
## Contributing in the Project
1. Select an issue in the area of your interest/expertise and try to complete it.
2. Solve the problems/errors reported by other contributors.
3. Any suggestions for adding features, modifications in the current model etc are welcome!
4. Help in creating a professional documentation for the entire project.
## How to complete the issues?
#### Selecting an issue
1. Read the objectives,goals milestones and other details before selecting the issue.
2. Post a comment in the issue you want to work on.
#### Completing the assigned task.
1. Clone the repository.
2. Make a branch and give branch name same as the issue you are working on.
3. Complete the tasks.
4. In case of codes, test it [here](https://hub.docker.com/r/shunyaos/shunya-armv7).
#### Objectives and goals
1. After the tasks are complete, verify if they meet the requirements and fulfill the goals.
2. Modify the work if necessary.
3. Make a merge request.
#### Issue completion
1. After the work is reviewed and accepted, it is merged by the maintainer.
2. You can now take the next issue and work on it!
## Deliverables
- Android Application




